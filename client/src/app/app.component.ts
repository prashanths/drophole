import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public readonly title = 'Drophole';

  public readonly files: Observable<any[]>;

  public uploading = false;
  public uploadPercent: number;

  public contentUrl;

  constructor(protected db: AngularFirestore, protected storage: AngularFireStorage, protected sanitizer: DomSanitizer) {
    this.files = db.collection('files', ref => ref.orderBy('uploaded', 'desc').limit(50)).valueChanges();
  }

  public async onFilesAdded(files) {
    if (files.length === 0 || this.uploading) {
      return;
    }
    const file = files[files.length - 1];

    this.uploading = true;
    this.uploadPercent = 0;

    const storageRef = this.storage.ref(file.name);
    const task = this.storage.upload(file.name, file);
    task.percentageChanges().subscribe(percent => {
      this.uploadPercent = percent;
    });

    task.snapshotChanges().pipe(
      finalize(async () => {
        this.uploading = false;
        const downloadUrl = await storageRef.getDownloadURL().toPromise();
        // insert into db...
        const fsRef = this.db.createId();
        this.db.collection('files').doc(fsRef).set({
          filename: file.name,
          data: downloadUrl,
          uploaded: new Date(),
        });
      }),
    )
    .subscribe();
  }

  public setContent(file) {
    this.contentUrl = this.sanitizer.bypassSecurityTrustResourceUrl(file.data);
  }

}
